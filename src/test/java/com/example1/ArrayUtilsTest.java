package com.example1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArrayUtilsTest {

    @Test
    public void testCalculateAverage() {
        int[] array = {1, 2, 3, 4, 5};
        double expectedAverage = 3.0;

        double actualAverage = ArrayUtils.calculateAverage(array);


        Assertions.assertEquals(expectedAverage, actualAverage, 0.01);
    }

    @Test
    public void testCalculateAverageWithEmptyArray() {
        int[] emptyArray = {};


        Assertions.assertThrows(IllegalArgumentException.class, () -> ArrayUtils.calculateAverage(emptyArray));
    }

    @Test
    public void testIsSquareMatrix() {
        int[][] squareMatrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] nonSquareMatrix = {
                {1, 2, 3},
                {4, 5, 6}
        };


        Assertions.assertTrue(ArrayUtils.isSquareMatrix(squareMatrix));
        Assertions.assertFalse(ArrayUtils.isSquareMatrix(nonSquareMatrix));
    }

    @Test
    public void testIsSquareMatrixWithEmptyMatrix() {
        int[][] emptyMatrix = {};

        Assertions.assertThrows(IllegalArgumentException.class, () -> ArrayUtils.isSquareMatrix(emptyMatrix));
    }
}