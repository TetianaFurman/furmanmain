package homework20;

public class PriceForDrinks {
    public static final double COFFEE_PRICE = 24.5;
    public static final double TEA_PRICE = 15.0;
    public static final double LEMONADE_PRICE = 35.50;
    public static final double MOJITO_PRICE = 40.0;
    public static final double MINERAL_WATER_PRICE = 17.50;
    public static final double COCA_COLA_PRICE = 24.2;
}