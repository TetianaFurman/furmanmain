package homework20;

public enum DrinksList {
    COFFEE,
    TEA,
    LEMONADE,
    MOJITO,
    MINERAL_WATER,
    COCA_COLA
}

