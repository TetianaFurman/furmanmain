package homework20;

import java.util.Scanner;

public class DrinksMachineApp {
    private static double totalCost = 0;
    private static int totalDrinks = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Введіть напій (COFFEE, TEA, LEMONADE, MOJITO, MINERAL_WATER, COCA_COLA) або 'EXIT' для завершення:");
            String input = scanner.nextLine().toUpperCase();

            if (input.equals("EXIT")) {
                break;
            }

            try {
                DrinksList drinksMachine = DrinksList.valueOf(input);
                makeDrink(drinksMachine);
            } catch (IllegalArgumentException e) {
                System.out.println("Напій не знайдено. Спробуйте ще раз.");
            }
        }

        System.out.println("Ви приготували " + totalDrinks + " напоїв.");
        System.out.println("Загальна вартість замовлення: " + totalCost + " грн.");
    }

    private static void makeDrink(DrinksList drinksMachine) {
        switch (drinksMachine) {
            case COFFEE:
                prepareCoffee();
                break;
            case TEA:
                prepareTea();
                break;
            case LEMONADE:
                prepareLemonade();
                break;
            case MOJITO:
                prepareMojito();
                break;
            case MINERAL_WATER:
                prepareMineralWater();
                break;
            case COCA_COLA:
                prepareCocaCola();
                break;
            default:
                System.out.println("Невірний вибір напою.");
        }
    }

    private static void prepareCoffee() {
        System.out.println("Готуємо каву. Ціна: " + PriceForDrinks.COFFEE_PRICE + " грн.");
        totalCost += PriceForDrinks.COFFEE_PRICE;
        totalDrinks++;
    }

    private static void prepareTea() {
        System.out.println("Готуємо чай. Ціна: " + PriceForDrinks.TEA_PRICE + " грн.");
        totalCost += PriceForDrinks.TEA_PRICE;
        totalDrinks++;
    }

    private static void prepareLemonade() {
        System.out.println("Готуємо лимонад. Ціна: " + PriceForDrinks.LEMONADE_PRICE + " грн.");
        totalCost += PriceForDrinks.LEMONADE_PRICE;
        totalDrinks++;
    }

    private static void prepareMojito() {
        System.out.println("Готуємо мохіто. Ціна: " + PriceForDrinks.MOJITO_PRICE + " грн.");
        totalCost += PriceForDrinks.MOJITO_PRICE;
        totalDrinks++;
    }

    private static void prepareMineralWater() {
        System.out.println("Готуємо мінеральну воду. Ціна: " + PriceForDrinks.MINERAL_WATER_PRICE + " грн.");
        totalCost += PriceForDrinks.MINERAL_WATER_PRICE;
        totalDrinks++;
    }

    private static void prepareCocaCola() {
        System.out.println("Готуємо кока-колу. Ціна: " + PriceForDrinks.COCA_COLA_PRICE + " грн.");
        totalCost += PriceForDrinks.COCA_COLA_PRICE;
        totalDrinks++;
    }
}