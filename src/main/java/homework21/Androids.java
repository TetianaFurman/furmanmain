package homework21;

public class Androids implements Smartphones, LinuxOS {
    @Override
    public void call() {
        System.out.println("Android: Making a call");
    }

    @Override
    public void sms() {
        System.out.println("Android: Sending an SMS");
    }

    @Override
    public void internet() {
        System.out.println("Android: Browsing the internet");
    }

    @Override
    public void runLinuxCommands() {
        System.out.println("Android: Running Linux commands");
    }
}

