package homework21;

public class MainApp {
    public static void main(String[] args) {
        Smartphones androidPhone = new Androids();
        Smartphones iPhone = new iPhones();

        System.out.println("--- Android Phone ---");
        androidPhone.call();
        androidPhone.sms();
        androidPhone.internet();

        if (androidPhone instanceof LinuxOS) {
            LinuxOS androidLinux = (LinuxOS) androidPhone;
            androidLinux.runLinuxCommands();
        }

        System.out.println("--- iPhone ---");
        iPhone.call();
        iPhone.sms();
        iPhone.internet();

        if (iPhone instanceof iOS) {
            iOS iPhoneiOS = (iOS) iPhone;
            iPhoneiOS.appStore();
        }
    }
}

