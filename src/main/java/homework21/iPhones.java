package homework21;

public class iPhones implements Smartphones, iOS {
    @Override
    public void call() {
        System.out.println("iPhone: Making a call");
    }

    @Override
    public void sms() {
        System.out.println("iPhone: Sending an SMS");
    }

    @Override
    public void internet() {
        System.out.println("iPhone: Browsing the internet");
    }

    @Override
    public void appStore() {
        System.out.println("iPhone: Accessing the App Store");
    }
}

