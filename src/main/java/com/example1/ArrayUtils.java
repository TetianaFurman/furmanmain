package com.example1;

    public class ArrayUtils {
        public static double calculateAverage(int[] array) {
            if (array == null || array.length == 0) {
                throw new IllegalArgumentException("Масив не повинен бути пустим");
            }

            int sum = 0;
            for (int num : array) {
                sum += num;
            }

            return (double) sum / array.length;
        }

        public static boolean isSquareMatrix(int[][] matrix) {
            if (matrix == null || matrix.length == 0) {
                throw new IllegalArgumentException("Матриця не повинна бути пустою");
            }

            int rows = matrix.length;
            for (int[] row : matrix) {
                if (row == null || row.length != rows) {
                    return false;
                }
            }

            return true;
        }
    }